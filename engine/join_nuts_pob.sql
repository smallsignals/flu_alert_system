create table flu_nuts_pob as SELECT  
	ST_Intersection(b.geom, p.wkb_geometry) As geom_intersc, b.*, p.*
	FROM flu_nuts b INNER JOIN poblacion_1km p ON ST_Intersects(b.geom,p.wkb_geometry)
	WHERE ST_Overlaps(b.geom, p.wkb_geometry);

