-- drop table pob_air ;
-- create table pob_air as SELECT  
--    ST_Intersection(b.geom, p.geom) As geom, t_total as pop , pm10_eea as air
--	FROM air_pm10 b INNER JOIN poblacion p ON ST_Intersects(b.geom,p.geom)
--	WHERE b.geom && p.geom and p.stat_levl_ = 3 ;
--	CREATE INDEX gix_pobair ON pob_air USING GIST (geom);

drop table pob_air_gfs ;
create table pob_air_gfs as SELECT  
	ST_Intersection(b.geom, p.wkb_geometry) As geom,pop, air, p.hum
	FROM pob_air b INNER JOIN gfs p ON ST_Intersects(b.geom,p.wkb_geometry)
	WHERE b.geom && p.wkb_geometry;
	CREATE INDEX gix_pobairgfs ON pob_air_gfs USING GIST (geom);
drop table pob_air_gfs_flu_nuts ;

create table pob_air_gfs_flu_nuts as SELECT           
    b.geom  
    As geom,pop, air, hum,t, t1,t2,t3,t53, id_nuts, week, year                      FROM pob_air_gfs b INNER JOIN flu_nuts p ON (ST_Intersects(b.geom,p.geom));
CREATE INDEX gix_pobairgfsflunuts ON pob_air_gfs_flu_nuts USING GIST (geom);
ALTER TABLE pob_air_gfs_flu_nuts ADD COLUMN id serial primary key;

--drop table pob_air_gfs_corine ;
--create table pob_air_gfs_corine as SELECT  
--	ST_Intersection(b.geom, p.geom) As geom,pop, air, hum, code_00 as corine
--	FROM pob_air_gfs b INNER JOIN corine p ON ST_Intersects(b.geom,p.geom)
--	WHERE b.geom && p.geom;
--	CREATE INDEX gix_pobairgfs ON pob_air_gfs USING GIST (geom);a


