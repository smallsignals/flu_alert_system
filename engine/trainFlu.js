#!/usr/bin/node
var numeric = require('numeric');
var pgp = require('pg-promise')({
    // Initialization Options
});
var Q = require ('q');
  var fs = require('fs');
var connection = {
    host: 'localhost',
    port: 5432,
    database: 'qol',
    user: 'web',
    password: 'web'
};


var db = pgp(connection);
var ws = fs.createWriteStream('file2.txt');
ws.write("[")
db.many("SELECT nuts_id from nuts where stat_levl_=0")
.then(function(country){
   console.log(country);
   var forp = [];
   for (var c =0; c < country.length; c++){
     //closure for promise
     (function(){
      var def = Q.defer();
      forp.push(def.promise);
      var lcountry = country[c];
      console.log("start country: "+lcountry.nuts_id)
      db.many("select id_nuts, year, week, pop, air, hum, t, t1, t2, t3,t53 from pob_air_gfs_flu_nuts where id_nuts = $1 order by year desc, week desc", [lcountry.nuts_id])
        .then(function(data){
	//console.log(JSON.stringify(data));
          console.log("ended country: "+lcountry.nuts_id);
	  var matrix =[];
          var y = [];
	  if (data.length<10) {
		console.log("not enough rows");
		def.resolve({});
		return;
	  }
          for ( var i = 0; i < data.length; i++){
		if (data[i].pop &&
                           data[i].air &&
                           data[i].hum &&
                           data[i].t1 &&
                           data[i].t2 &&
                           data[i].t3 &&
                           data[i].t53	)
		{
		   var row = [data[i].pop, 
			   data[i].air,
                           data[i].hum,
			   data[i].t1,
                           data[i].t2,
                           data[i].t3,
                           data[i].t53];
		    matrix.push(row);
                    y.push(data[i].t);
                }
		
	  }
	  //console.log(matrix);
	  //console.log(y);
	  console.log("transposing")
	  var mt = numeric.transpose(matrix);
          console.log("product 1");
	  var mtm = numeric.dot(mt, matrix);
          console.log("inverting");
	  var invmtm = numeric.inv(mtm);
          console.log("product 2");
          var pinv = numeric.dot(invmtm, mt);
	  console.log("solving");
	  var coef = numeric.dot(pinv, y);
	  console.log(data[0].id_nuts);
	  console.log(coef);
          def.resolve({"nuts":data[0].id_nuts,"coef":coef});
	  ws.write(JSON.stringify({"nuts":data[0].id_nuts,"coef":coef})+",");
        })
        .catch(function (error) {
           console.log("ERROR:", error);
           def.reject(error);
        });
       })() // fin closure dentro dle for
    
    }// fin for 
    
    Q.all(forp).then(function(data){
        ws.write("]");
        ws.end();
        console.log("end train. saving data..");
	fs.writeFile("train_result.json", 
        JSON.stringify(data), 
        function(err) {
             if(err) {
               return console.log(err);
             }
           // console.log("The file was saved!");
        }); 
    });
});
