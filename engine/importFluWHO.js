#!/usr/bin/node
var fs = require('fs');
var pgp = require('pg-promise')({
    // Initialization Options
});
var process = require ('process');
var Q = require('q');

var connection = {
    host: 'localhost',
    port: 5432,
    database: 'qol',
    user: 'web',
    password: 'web'
};
process.on('uncaughtException', function (exception) {
  console.log(exception); // to see your exception details in the console
  // if you are on production, maybe you can send the exception details to your
  // email as well ?
});

var db = pgp(connection);

db.none("delete from flu").
then(function(){
    console.log("finding country list");
    return db.many("select nuts_id, eng_name from nuts where stat_levl_ = 0")
})
.then(function(nuts){
    console.log ("reading file : " + process.argv[2]);
    var filename = process.argv[2];
    var buf = fs.readFileSync(filename, "utf8").toString().split("\n");;
    var forp = []
    for (var i = 3; i < buf.length; i++){
	var line = buf[i].split(",");
//        console.log("line: "+i+" "+line);
	//console.log(line);
        var swabs = parseInt(line[8]);
        if (isNaN(swabs)) swabs = -1;
	if (swabs==null) swabs = -1; 
        var possitive = parseInt(line[19]);
        if (isNaN(possitive)) possitive = 0;
        if (possitive==null) possitive=0;
	var country = line[0]; 
	var ratio = 0; 
	if (swabs > 0 ) ratio = possitive / swabs; 
	var found = 0;
	//console.log("possitive: "+ possitive + " swabs " + swabs + " ratio " + ratio ); 
        //console.log("looking for country "+country); 
	//console.log (JSON.stringify(nuts));
        for (var j = 0; j < nuts.length; j++){
	//	console.log("nuts: "+ nuts[j].nuts_id+ ":" + nuts[j].eng_name);
		if (nuts[j].eng_name== country){
			country = nuts[j].nuts_id;
			found = 1; 
			break;
		}
        }
	if (found == 0 ) {
		//console.log ("not found: " + country);
	}else{
            //closrue for the fromise
            (function(){
		var def = Q.defer();
                var idx = i;
		forp.push(def.promise);
                console.log ("inserting");
        	db.none("insert into flu (id_nuts, year, week,swabs, positive_percent,spread) values($1, $2, $3, $4, $5, $6)", 
                	[country,line[3],line[4],swabs,ratio,line[20]]) 
    		.then(function () {
        		// success;a
		    def.resolve();
   	     	    console.log("inserted "+idx);
    		})
    		.catch(function (error) {
		    def.reject(error);
    		    console.log(JSON.stringify(error));
    		});
             })();
	}
    }//end for
    return Q.all(forp);
    
/*}).then(function(){
       console.log("joining geometries");
       return db.none("UPDATE flu_nuts AS v SET geom = s.geom FROM nuts AS s WHERE v.id_nuts = s.nuts_id ");
}).then(function(){
	console.log("analyse");
	return deb.none("ANALYZE flu_nuts");*/
}).then(function(){
	console.log("end");
	process.exit();
}).catch(function (error) {
        console.log("error:"+JSON.stringify(error));
	process.exit();
});


