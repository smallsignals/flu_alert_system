#!/usr/bin/node
var numeric = require('numeric');
var pgp = require('pg-promise')({
    // Initialization Options
});

var connection = {
    host: 'localhost',
    port: 5432,
    database: 'qol',
    user: 'web',
    password: 'web'
};


var db = pgp(connection);

db.one("SELECT $1 AS value", 123)
.then(function (data) {
//        console.log("DATA:", data.value);
    })
.then(function(data){
	return db.many("select flu.id_nuts, positive_percent, year, week from flu, nuts where flu.id_nuts = nuts.nuts_id order by nuts.nuts_id, flu.year asc, flu.week asc")
})
.then(function(data){
	//console.log(JSON.stringify(data));

	var matrix =[];
        var y = [];
	var year = new Array(53).fill(0);
	var nuts = data[0].id_nuts;
        console.log("delete from flu_nuts;");
        console.log("begin;");
        for ( var i = 3; i < data.length; i++){
		if (nuts != data[i].id_nuts){
			year =new Array(53).fill(0);
			nuts = data[i].id_nuts;
		}
		
		var row = [data[i-1].positive_percent, 
			   data[i-2].positive_percent,
                           data[i-3].positive_percent,
			   year[data[i].week-1]];
		matrix.push(row);
                y.push(data[i].positive_percent);
	
		year[data[i].week-1]=data[i].positive_percent;
		
		console.log("insert into flu_nuts (id_nuts,t, t1,t2,t3,t53,week, year) values ('"+data[i].id_nuts+"', "+data[i].positive_percent+", "+row[0]+", "+row[1]+", "+row[2]+", "+row[3] +", "+data[i].week +", "+data[i].year +");");
	}
	console.log("commit;");
console.log("UPDATE flu_nuts AS v SET geom = s.geom FROM nuts AS s WHERE v.id_nuts = s.nuts_id; ");	
	console.log("analyze flu_nuts;");
	//console.log(matrix);
	//console.log(y);
/*	console.log("transposing")
	var mt = numeric.transpose(matrix);
        console.log("product 1");
	var mtm = numeric.dot(mt, matrix);
        console.log("inverting");
	var invmtm = numeric.inv(mtm);
        console.log("product 2");
        var pinv = numeric.dot(invmtm, mt);
	console.log("solving");
	var coef = numeric.dot(pinv, y);
	console.log(coef);
*/
})
  .catch(function (error) {
        console.log("ERROR:", error);
    });

