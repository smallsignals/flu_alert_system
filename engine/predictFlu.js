#!/usr/bin/node
var numeric = require('numeric');
var pgp = require('pg-promise')({
    // Initialization Options
});
var Q = require('q');
var fs = require('fs');
var process = require('process');
var train = require('./train_result.json');

var connection = {
    host: 'localhost',
    port: 5432,
    database: 'qol',
    user: 'web',
    password: 'web'
};
var week = process.argv[2];
var year = process.argv[3];
var db = pgp(connection);
db.none("create table if not exists week"+week+" as select * from week_proto limit 1")
.then(function(){
console.log("borrando");
return db.none("delete from week"+week+";");
}).then(function(){

var forp = [];
for (var t = 0; t < train.length; t++){
     console.log(train[t].nuts);
     //closure para el for
     (function(){
     var lcountry = train[t].nuts;
     var coef = train[t].coef;
     if (coef){
       var def = Q.defer();
       forp.push(def.promise);
       db.many("select id_nuts, year, week, pop, air, hum, t, t1, t2, t3,t53,geom from pob_air_gfs_flu_nuts where id_nuts = $1 and week = $2 and year = $3", [lcountry,week, year])
       .then(function(data){
	//console.log(JSON.stringify(data));
          console.log("processing: "+ lcountry);
	  console.log("data length:"+ data.length);
	  var matrix =[];
          var y = [];
          for ( var i = 0; i < data.length; i++){
		console.log(data[i]);
		console.log("1");
	      if (data[i].pop != null&&
                           data[i].air != null&&
                           data[i].hum != null&&
                           data[i].t1 != null&&
                           data[i].t2 != null&&
                           data[i].t3 != null&&
                           data[i].t53 != null )
              {

		var row = [data[i].pop, 
			   data[i].air,
                           data[i].hum,
			   data[i].t1,
                           data[i].t2,
                           data[i].t3,
                           data[i].t53];
		matrix.push(row);
                y.push(data[i].t);
		 console.log("2:"+coef+" r:"+row);
		var tpredict = numeric.dot(coef, row);
		//console.log("real : " +data[i].t+ "predicted : " + tpredict); 
		 console.log("3");
		db.none("insert into week"+week+" (t,pop, air, hum, t1,t2,t3,t53,geom, year, week, id_nuts) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)",[
			   tpredict,
			   data[i].pop,
                           data[i].air,
                           data[i].hum,
                           data[i].t1,
                           data[i].t2,
                           data[i].t3,
                           data[i].t53,data[i].geom,
			   data[i].year,data[i].year, data[i].id_nuts]).catch(function(error){console.log("error insert:"+ error)});
	     }//end if something null 
console.log("4");
	  }// end for
          console.log("end processing : "+lcountry);
  	  def.resolve();
        })
        .catch(function (error) {
            console.log("ERROR catch:"+ error+" country: " + lcountry);
        });
      }//end if coef
   })();//fin closure
}// end for
return Q.all(forp)
}).then(function(){
	console.log("end program");
}).catch(function (error){
	console.log("error:"+error);
	process.exit();

});
;

