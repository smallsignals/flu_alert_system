#!/usr/bin/node
var fs = require('fs');
var pgp = require('pg-promise')({
    // Initialization Options
});
var process = require ('process');
var Q = require('q');
var moment = require('moment');
var connection = {
    host: 'localhost',
    port: 5432,
    database: 'qol',
    user: 'web',
    password: 'web'
};
process.on('uncaughtException', function (exception) {
  console.log(exception); // to see your exception details in the console
  // if you are on production, maybe you can send the exception details to your
  // email as well ?
});

var db = pgp(connection);

var http = require('http');
var fs = require('fs');
var exec = require('child_process').exec;

var thismonth = moment().add(-1, 'days').format('YYYYMM');
var thisday = moment().add(-1, 'days').format('YYYYMMDD');
var gfsfile = "gfs_4_"+thisday+"_0000_072.grb2"
var file = fs.createWriteStream(gfsfile);

var urlgfs = "http://nomads.ncdc.noaa.gov/data/gfs4/"+thismonth+"/"+thisday+"/"
var defered = Q.defer();
var request = http.get(urlgfs+gfsfile, function(response) {
  response.pipe(file).on('finish',function(){
      defered.resolve();
  });
});
defered.promise.then(function(){
        console.log("quantify");
	var defered2 = Q.defer();
	exec("gdal_translate -of GTIFF "+gfsfile+" -ot uint32 temp.tif -scale 0 0.002 0 200 -b 281", function (error, stdout, stderr){
		console.log(stdout+" "+stderr);	
		defered2.resolve(error);
	});
	return defered2.promise;

}).then(function(){
	console.log("polygonyze"); 
var defered2 = Q.defer();
        exec("gdal_polygonize.py temp.tif -f PostgreSQL PG:\"dbname=qol host='localhost' port='5432' user='web' password='web'\" gfs hum", function (error, stdout, stderr){
            console.log(stdout+" "+stderr);
            defered2.resolve(error);
        });
        return defered2.promise;
}).then(function(){
    console.log("shifting");
    return db.none("UPDATE gfs set wkb_geometry =  ST_Shift_Longitude(wkb_geometry);")
}).then(function(){
    console.log("deleting deformed polygons");
    return db.none("delete from gfs where  wkb_geometry && ST_GeomFromText('POLYGON((177.9 -100 ,177.9 100,181 100, 181 101, 177.9 -100))',4258)")
}).then(function(){
        console.log("end");
        process.exit();
}).catch(function (error) {
        console.log("error:"+JSON.stringify(error));
        process.exit();
});



