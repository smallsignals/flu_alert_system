//api.js

var Api = function() {
    this.server = SERVER_URL_BASE;
    this.metadata =[,,,,,]
};

Api.prototype.getMe = function(fb) {
    $.ajax({
        url: this.server + '/users/me'+ "?access_token=" + SERVER_TOKEN,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(data) {
						fb(data);
        },
        error: function(jqXHR) {
            if (jqXHR.status == 401) {
                console.log('Invalid user or password');
                alert('Invalid User or password');
            } else {
                console.log('Error connecting to server');
                alert('Error connecting to server');
            }
        },
        failure: function(errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}
Api.prototype.getUsers = function(fb) {
    $.ajax({
        url: this.server + '/users',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(data) {
						fb(data);
        },
        error: function(jqXHR) {
            if (jqXHR.status == 401) {
                console.log('Invalid user or password');
                alert('Invalid User or password');
            } else {
                console.log('Error connecting to server');
                alert('Error connecting to server');
            }
        },
        failure: function(errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

  var contrib_func= function(event){
    event.preventDefault();

    var inputs= $(".symptomschecks");
    //var inputs = fieldset[0].children("input");
    var attr = {};
    for (var i = 0; i < inputs.length; i++){
      attr[inputs[i].name]=inputs[i].checked;
    }
    console.log(attr);
    $.ajax({
      url: SERVER_URL_BASE + '/api/users/update',
      type: 'POST',
      data: JSON.stringify({"user":{"attr":attr}}),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      beforeSend : function( xhr ) {
      	xhr.setRequestHeader( 'Authorization', 'BEARER ' + SERVER_TOKEN  );
      },
      success: function(data){
        alert("Saved");
        $.mobile.navigate("#tools");
      },
      error: function() {
        console.log('Error connecting to server');
        alert('Error connecting to server');
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });

    $.mobile.navigate("#tools");
  }
  var enviro_func= function(event){
    event.preventDefault();

    var inputs= $(".symptomschecks");
    //var inputs = fieldset[0].children("input");
    var enviro = {};
    for (var i = 0; i < inputs.length; i++){
      enviro[inputs[i].name]=inputs[i].checked;
    }
    console.log(attr);
    $.ajax({
      url: SERVER_URL_BASE + '/api/users/update_enviro',
      type: 'POST',
      data: JSON.stringify({"user":{"enviro":enviro}}),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      beforeSend : function( xhr ) {
      	xhr.setRequestHeader( 'Authorization', 'BEARER ' + SERVER_TOKEN  );
      },
      success: function(data){
        alert("Saved");
        $.mobile.navigate("#tools");
      },
      error: function() {
        console.log('Error connecting to server');
        alert('Error connecting to server');
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });

    $.mobile.navigate("#tools");
  }

  var signup_func = function(){
    var userData = {"username": $("#username_txt").val(),
                    "password": $("#password_txt").val(),
                    "email": $("#email_txt").val()};
    console.log(userData);
    $.ajax({
      url: SERVER_URL_BASE + '/api/users',
      type: 'POST',
      data: JSON.stringify(userData),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      //beforeSend : function( xhr ) {
      //	xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
      //},
      success: function(data){
        $.mobile.navigate("#login_page");
      },
      error: function() {
        console.log('Error connecting to server');
        alert('Error connecting to server');
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });
    return false;
  }


  var login_func = function(event){
    console.log("login func");
    event.preventDefault();
    var user = $("#user_login_txt").val();
    var pass = $("#pass_login_txt").val();
    localStorage.user = user;
    localStorage.pass = pass;
    $.ajax({
      url: SERVER_URL_BASE + '/api/auth/basic',
      type: 'GET',
      //data: {},JSON.stringify(userData),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      beforeSend: function (xhr) {
        xhr.setRequestHeader ("Authorization", "Basic " + btoa(user + ":" + pass));
      },

      //beforeSend : function( xhr ) {
      //	xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
      //},
      success: function(data){
        SERVER_TOKEN=data.token;
        $.mobile.navigate("#main_page");
      },
      error: function(jqXHR) {
        if (jqXHR.status == 401){
          console.log('Invalid user or password');
          alert('Invalid User or password');
        }else{
          console.log('Error connecting to server');
          alert('Error connecting to server');
        }
      },
      failure: function(errMsg) {
        console.log(errMsg);
        alert(errMsg);
      }
    });
  return false;
  }//endlogin button
