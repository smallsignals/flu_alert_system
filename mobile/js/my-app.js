// Initialize your app
/*var myApp = new Framework7({
    animateNavBackIcon: true,
    // Enable templates auto precompilation
    precompileTemplates: true,
    // Enabled pages rendering using Template7
	swipeBackPage: true,
	pushState: true,
    template7Pages: true
});*/

// Export selectors engine
//var $$ = Dom7;

// Add main View
//var mainView = myApp.addView('.view-main', {
// Enable dynamic Navbar
//   dynamicNavbar: false,
//});
var map;
$( "#map_page" ).on( "pagecontainerchange", function() {
  map.updateSize();
  console.log("map_page change");
});
$( "#map_page" ).on( "pageshow", function( event, ui ) {console.log("caca de la vaca");
                                                        map.updateSize();
                                                       } );

$('#map_page').one("pageinit", function(){
  console.log("map_page pageinit2")

  var screen = $.mobile.getScreenHeight();
  var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight()  - 1 : $(".ui-header").outerHeight();
  header = $("#navbardiv").outerHeight();
  var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();

  /* content div has padding of 1em = 16px (32px top+bottom). This step
		can be skipped by subtracting 32px from content var directly. */
  var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();

  var content = screen - header - footer - contentCurrent;
  $("#map").height(content);
  $("#map").width($( window ).width());
  console.log(content)
  $(".ui-content").height(content);
  var extent = [-16.1, 32.88, 39.65, 84.17];
  var projection = new ol.proj.Projection({
    code: 'EPSG:4258',
    units: 'degrees',
    extent:extent
  });
  var startResolution = ol.extent.getWidth(extent) / 256;
  var resolutions = new Array(22);
  for (var i = 0, ii = resolutions.length; i < ii; ++i) {
    resolutions[i] = startResolution / Math.pow(2, i);
  }
  var tileGrid = new ol.tilegrid.TileGrid({
    extent: extent,
    resolutions: resolutions,
    tileSize: [512, 512]
  });

  map = new ol.Map({
    controls: ol.control.defaults().extend([
      new ol.control.ScaleLine({
        units: 'metric'
      })
    ]),

    target: 'map',
    layers: [
/*      new ol.layer.Tile({
        source: new ol.source.OSM()
      }),*/

      new ol.layer.Tile({
        extent: extent,
        source: new ol.source.TileWMS({
          url: 'http://138.100.101.64/flash//cgi-bin/mapserv?map=/var/opt/flash/server/osm_mapfile.map&',
          params: {'LAYERS': 'ili','VERSION':'1.1.0'},
          serverType: 'mapserver',
          projection:projection,
          tileGrid: tileGrid
        })
      })

    ],
    view: new ol.View({
      center: [0.23, 46.86],
      zoom: 2,
      projection: projection,
      extent:extent
    }),
     tileGrid: tileGrid
  });

  map.updateSize();

});

var update_plot = function(pais ,caption){
  $.getJSON('http://138.100.101.64/flash/api/'+pais, function (d) {
    var data=[];
    for (var i = 0; i < d.length ; i++){
      var dd = d[i];
      var ddd = (1 + (dd.week - 1) * 7); // 1st of January + 7 days for each week

      resultd = new Date(dd.year, 0, ddd);
      data.push([resultd.getTime(), dd.t]);
    }
    // Create the chart
    $('#container').highcharts('StockChart', {
      rangeSelector: {
        selected: 1
      },
      title: {
        text: caption
      },
      series: [{
        name: 'Possitves ILI',
        data: data,
        tooltip: {
          valueDecimals: 2
        }
      }]
    });
  });
}

$('#plot_page').one("pageinit", function(){
  console.log("plot_page pageinit2")
  $.getJSON('http://138.100.101.64/flash/api/countries', function (data) {
    $.each(data,function(key, value){
      $("#plot_country_select").append('<option value=' + value.nuts_id + '>' + value.eng_name + '</option>');
    });
  })
  $('select').on('change', function() {
    update_plot( this.value , this.text); // or $(this).val()
  });

  update_plot("ES", "Spain");
});

$("#about_page").one('pageinit',function(){
  $("#enviro_button").click(enviro_func);
});

$("#crowd_page").one('pageinit',function(){
  $("#contrib_button").click(contrib_func);
});

$("#login_page").one('pageinit', function (e) {

  /*document.addEventListener('touchmove', function(event) {
	   if(event.target.parentNode.className.indexOf('navbarpages') != -1 || event.target.className.indexOf('navbarpages') != -1 ) {
		    event.preventDefault(); }
	}, false);*/
  console.log("pageInit");
  $("#signup_button").click(signup_func);
  $("#login_button").click(login_func);//endlogin button

  if (localStorage.user){
	var user = $("#user_login_txt").val(localStorage.user);
	var pass = $("#pass_login_txt").val(localStorage.pass);
  }else{
	  $.mobile.navigate("#firststeps");
  }

  //map.updateSize();

  //console.log("map_page change");

})
